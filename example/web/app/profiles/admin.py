# -*- coding: utf-8 -*- 

from django.contrib import admin
from models import *

class ProfileAdmin(admin.ModelAdmin):
    list_display=tuple([f.name for f in Profile._meta.fields])
admin.site.register(Profile,ProfileAdmin)

