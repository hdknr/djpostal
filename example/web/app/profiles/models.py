# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User
from django.utils.timezone import now

class Name(models.Model):

    family_name = models.CharField(max_length=20)
    first_name  = models.CharField(max_length=20)
    family_kana = models.CharField(max_length=20)
    first_kana  = models.CharField(max_length=20)

    class Meta:
        abstract =True

class Address(models.Model):
    zip = models.CharField(max_length=7)
    prefecture= models.CharField(max_length =50 ,)
    city = models.CharField(max_length =50 ,)
    town = models.CharField(max_length =50 ,)
    street = models.CharField(max_length =70 ,)
    building= models.CharField(max_length =70 ,blank=True,default=None,null=True,)

    class Meta:
        abstract =True

class Profile(Name,Address):
    user = models.ForeignKey(User, null=True,blank=True,default=None)
    session_key = models.CharField(max_length=100, )
    email = models.EmailField() 
    phone = models.CharField(max_length=12 )
    created_at = models.DateTimeField(auto_now_add=True)
