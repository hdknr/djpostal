# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

urlpatterns = patterns('djpostal.views',
    url(r'json/zip/(?P<maxnum>\d*)','json_zip',name='djpostal_json_zip',),
)
