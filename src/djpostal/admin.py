# -*- coding: utf-8 -*- 
from django.contrib import admin
from models import *

class PrefectureAdmin(admin.ModelAdmin):
    list_display=tuple([f.name for f in Prefecture._meta.fields])

admin.site.register(Prefecture,PrefectureAdmin )

class JpAddressAdmin(admin.ModelAdmin):
    list_display=tuple([f.name for f in JpAddress._meta.fields])

admin.site.register(JpAddress,JpAddressAdmin )

