''' 
'''
from django.conf import settings
from django.utils.encoding import force_unicode
from django.core import serializers

import csv
import pykf

__all__ = ('CsvUnicodeWriter', 'CsvUnicodeReader','to_json',)

class CsvUnicodeWriter(object):
    def __init__(self, stream, dialect=None, encoding=None, errors="strict", **kwds):
        self.writer = csv.writer(stream, dialect=dialect or csv.excel, **kwds)
        self.encoding = encoding or settings.DEFAULT_CHARSET
        self.errors = errors

    def writerow(self, row):
        self.writer.writerow(
            map(lambda s: force_unicode(s, errors=self.errors).encode(self.encoding, self.errors), 
            row))

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)


def detect_encoding(stream):
    ret= {   
        pykf.UTF8   : 'utf-8',
        pykf.SJIS   : 'shift-jis',
#        pykf.ASCII  : settings.DEFAULT_CHARSET, #: dirty fix : shift-jis
    }.get( pykf.guess( stream.read() ),settings.DEFAULT_CHARSET)
    stream.seek(0)
    return ret

class CsvUnicodeReader(object):
    def __init__(self, stream, dialect=None, encoding=None, 
            errors="strict", force_strip = True,**kwds):
        self.reader = csv.reader(stream, dialect=dialect or csv.excel, **kwds)
        self.encoding = encoding  if encoding else detect_encoding(stream)
        self.line_num = 0 # Needed for DictReader
        self.errors = errors
        self.force_strip = force_strip

    def __iter__(self):
        return self

    def next(self):
        return map(lambda s: 
            force_unicode(s, encoding=self.encoding, errors=self.errors).strip() \
            if self.force_strip else \
            force_unicode(s, encoding=self.encoding, errors=self.errors), 
            self.reader.next())

    @classmethod
    def csv_enumerator(cls,stream, fieldnames=None,dialect=None, 
                            encoding=None, errors="strict", **kwds):
        ''' enumerator factory for CsvUnicodeReader

            - Key must be encode in unicode.
            - data_dict values must be "strip()"ed 
              because there could be heading and trailing spaces around.
    
        sample::

            for index,data_dict in CsvUnicodeReader.csv_enumerator(csv_straeam,encoding='utf8'):
                inits = dict( [ ( k.encode('ascii') , v ) for k,v in data_dict.iteritems() ]
                Account(**inits).save()
        '''
        csvreader = csv.DictReader(stream,fieldnames) 
        csvreader.reader = cls(stream, dialect=dialect, 
                            encoding=encoding, 
                                errors=errors , **kwds)
        return enumerate(csvreader)

from collections import Iterable
def to_json(data,ascii=True,indent=2,stream = None,file_name=None):
    ''' JSON fixture '''
    data = data if isinstance(data,Iterable) else [data]
    if stream == None and file_name != None : 
        stream = open(file_name, "w")
    if stream:
        serializers.serialize("json",data, ensure_ascii=ascii,
                indent=indent,stream = stream )
        stream.close()
    else:
        return serializers.serialize("json",data, ensure_ascii=ascii,indent=indent) 
