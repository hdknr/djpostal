# -*- coding: utf-8 -*-

from django.db import models

class Prefecture(models.Model):
    ''' 都道府県 '''
    name = models.CharField(max_length=50,db_index=True,unique=True,)   
    kana = models.CharField(max_length=50,db_index=True,unique=True,)

    class Meta:
        verbose_name =u'都道府県'
        verbose_name_plural =u'都道府県'
        
class JpAddress(models.Model):
    ''' JP郵便データ '''
    code = models.CharField(max_length=10,db_index=True,unique=False)
    zip5 = models.CharField(max_length=5 )
    zip  = models.CharField(max_length=7,db_index=True)
    prefecture_kana = models.CharField(max_length =50,db_index=True )
    city_kana = models.CharField(max_length =50 ,db_index=True)
    town_kana = models.CharField(max_length =80 ,db_index=True)
    prefecture= models.CharField(max_length =50 ,db_index=True)
    city = models.CharField(max_length =50 ,db_index=True)
    town = models.CharField(max_length =50 ,db_index=True)
    is_split = models.BooleanField() 
    is_small = models.BooleanField() 
    is_towncode = models.BooleanField() 
    is_multi  = models.BooleanField() 
    changed  = models.IntegerField()  # 0 = no changed , 1 = changed , 2 = discontinued
    reason   = models.IntegerField() 

    @classmethod
    def is_changed(cls,val):
        if type(val) == dict :
            if False == val.has_key('changed'):
                return False
            else:
                val = val['changed']    
        try:
            return int(val) == 1
        except:
            return False

    @classmethod
    def to_prefectures(cls):
        for i in  cls.objects.values('prefecture','prefecture_kana').distinct() :
            Prefecture(name=i['prefecture'],kana=i['prefecture_kana']).save()

    class Meta:
        verbose_name =u'JP郵便データ'
        verbose_name_plural =u'JP郵便データ'
