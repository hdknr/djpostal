# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from optparse import make_option
from datetime import datetime
from jcconv import *
import commands
import os
import csv
import sys
#
from djpostal.utils import *
from djpostal.models import JpAddress

class Command(BaseCommand):
    args = ''
    help = ''
    option_list = BaseCommand.option_list + (
        make_option('--url',
            action='store',
            dest='url',
            default='http://www.post.japanpost.jp/zipcode/dl/kogaki/lzh/ken_all.lzh',
            help='JP Address Data URL'),
        make_option('--file',
            action='store',
            dest='file',
            default='ken_all.csv',
            help='JP Address Data File Name'),

        make_option('--start',
            action='store',
            dest='start',
            default=0,
            help='start record'),

        make_option('--end',
            action='store',
            dest='end',
            default=sys.maxint,
            help='end record'),

        make_option('--charset',
            action='store',
            dest='charset',
            default='sjis',
            help='end record'),

        make_option('--force','-f',
            action='store_true',
            dest='force',
            help='force to do'),

        make_option('--prefectures','-p',
            action='store',
            dest='prefectures',
            default=5,
            help='end record'),
        )

    def download(self,*args,**options):
        cmd= "curl %(url)s | lha x - " %  options 
        if options['charset'] == 'utf8':
            cmd=cmd +  ";nkf -w %(file)s > %(file)s.utf8" % options 
            options['file'] = options['file'] + ".utf8"
        print "download",cmd
        print commands.getoutput( cmd )

    def handle_download(self, *args, **options):
        if os.path.exists( options['file'] ) and options['force'] ==False :
                print "using existing",options['file']
                return  
        self.download(*args,**options)

    def handle_load(self,*args, **options):
        self._update(self._update_load,*args,**options)  
    
    def handle_update(self,*args, **options):
        self._update(self._update_update,*args,**options)  

    def handle_testload(self,*args, **options):
        self.prefecture=""
        self.prefecture_count =0
        self.max_prefectures = int(options['prefectures'] )
        self._update(self._update_testload,*args,**options)  

    def _update_testload(self,data ):
        if self.prefecture != data['prefecture']: 
            self.prefecture = data['prefecture']
            self.prefecture_count = 0 
        
        self.prefecture_count =self.prefecture_count + 1

        if self.prefecture_count <= self.max_prefectures:
            JpAddress(**data).save() 
            self.counters[0] = self.counters[0]  + 1
        else:
            self.counters[2] = self.counters[2]  + 1

    def _update_load(self,data):
        JpAddress(**data).save() 
        self.counters[0] = self.counters[0]  + 1

    def _update_update(self,data):
        cindex = 2  #: passed
        if data_dict['changed'] != 0:
            zip = data_dict.pop('zip')
            obj,created = JpAddress.objects.get_or_create(
                              zip = zip,
                              defaults = data_dict,
                          )
            cindex = 0 if created else 1 
        self.counters[cindex] = self.counters[cindex]  + 1

    def _update(self, func, *args, **options):

        if os.path.exists( options['file'] ) == False :
            if options['force'] ==False :
                print options['file'], "doesn't exist. Download it first."
                return
            #: force to download
            self.download(*args,**options)             

        #:prepare
        options['start'] = int(options['start'])
        options['end'] = int(options['end'])

        dts = datetime.now()
        self.counters =  [ 0,0,0 ] 
        #: insert, updates, passed, 
        
        #: record precess
        for index,data_dict in CsvUnicodeReader.csv_enumerator(
                        open( options['file'] ),
                        fieldnames=  [ i.name for i in JpAddress._meta.fields ][1:],
                        encoding=options['charset'],
                        force_strip=True,
                        ):

            if index < options['start']:
                continue
            if index > options['end']:
                break
            
            for k,v in data_dict.items():
                if k.find('kana') >=0 :
                    data_dict[k] = half2hira(v) 

            func( data_dict )
#            try:
#                func( data_dict )
#            except Exception,e:
#                print "Error on ", index , e.message
#
        #: finally
        d = datetime.now() - dts

        print "Total=%d Insearts=%d Updates=%d Ignores=%d Elapsed=%d.%d"  % (
                self.counters[0]+self.counters[1],
                self.counters[0],self.counters[1],self.counters[2],
                d.seconds,d.microseconds)

    def handle_to_prefectures(self,*args,**options):
        JpAddress.to_prefectures()

    def handle(self, *args, **options):
        if len(args) > 0 :
            getattr(self, 'handle_%s'% args[0],self.handle_help)(*args,**options)
        else:
            self.handle_help(*args,**options )  

    def handle_help(self,*args,**options):
        print args,options
