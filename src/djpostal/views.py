# -*- coding: utf-8 -*-

from django.http import HttpResponse
from django.core import serializers

from models import JpAddress

def json_zip(request,maxnum):
    ''' '''
    maxnum = maxnum or 10
    zip = request.GET.get('term',None)
    qs =JpAddress.objects.filter()[:int(maxnum)] if zip == None or len(zip) <1 else \
        JpAddress.objects.filter(zip__startswith=zip)[:int(maxnum)]
         
    return  HttpResponse(
            serializers.serialize("json", qs, ensure_ascii=False), 
            mimetype='application/json; charset=utf-8'
            )
